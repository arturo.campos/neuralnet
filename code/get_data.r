#get data from bigquery
get_data <- function(){
  
  #install.packages("bigrquery")
  library("bigrquery")
  
  #project id for billing
  billing <- c("bigdata-demo-219916")
  
  #sql query
  sql <- "SELECT CAST(id as STRING) AS video_id, * FROM `bigdata-demo-219916.Rekognition.Master_Table`"
  
  #bq project query
  tb <- bq_project_query(billing, sql)
  
  #download data and save in variable
  #data needs to get out of the scope of the function.
  data <<- bq_table_download(tb)
  
  #review data structure if needed.
  #str(data)

}
